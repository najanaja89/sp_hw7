﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using unirest_net.http;

namespace sp_hw7
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void ResultButtonClick(object sender, RoutedEventArgs e)
        {

            Task<HttpResponse<string>> response = Unirest.get($"https://love-calculator.p.rapidapi.com/getPercentage?fname={hisName.Text}&sname={herName.Text}")
           .header("X-RapidAPI-Host", "love-calculator.p.rapidapi.com")
           .header("X-RapidAPI-Key", "d517114ac9msh8b68c4397d27b8ap15d805jsn08aa0da206d3")
           .asJsonAsync<string>();

          

            Task.Run(() =>
            {
                var jsonString = response.Result.Body.ToString();
                var result = JsonConvert.DeserializeObject<Names>(jsonString);
                MessageBox.Show($"Percent of compability: {result.Percentage}\nResult: {result.Result}");
            });
        }
    }
}
