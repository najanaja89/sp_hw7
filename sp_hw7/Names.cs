﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
namespace sp_hw7
{
    public partial class Names
    {
        public string Fname { get; set; }
        public string Sname { get; set; }
        public long Percentage { get; set; }
        public string Result { get; set; }

    }
}
